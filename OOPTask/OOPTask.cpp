﻿#include <iostream>
#include <chrono>

#include "Publication.h"
#include "Research.h"
#include "Student.h"


using namespace ooptask;

int main()
{
	std::cout << "Person:\n";
	Person person("Anton", "Brazhnyi", std::chrono::system_clock::now());
	person.printInfo();

	std::cout << "\nStudents:\n";
	Student student1("Anton", "Brazhnyi", std::chrono::system_clock::now(), 4);
	student1.printInfo();
	Student student2("Oleg", "Olegovych", std::chrono::system_clock::now(), 3);
	student2.printInfo();

	std::cout << "\nPublications:\n";
	Publication publication1(student1, ScientificAchievement::Abstract, std::chrono::system_clock::now());
	publication1.printInfo();
	Publication publication2(student1, ScientificAchievement::ScientificJournalArticle, std::chrono::system_clock::now() - std::chrono::days(5));
	publication2.printInfo();
	Publication publication3(student2, ScientificAchievement::ProfessionalPublicationArticle, std::chrono::system_clock::now() - std::chrono::days(10));
	publication3.printInfo();
	Publication publication4(student2, ScientificAchievement::ProfessionalPublicationArticle, std::chrono::system_clock::now() - std::chrono::days(15));
	publication4.printInfo();
	
	Research research1;
	std::deque<Publication> publications({ publication1, publication2, publication3, publication4 });
	Research research2(publications, "Generative diffusion models");
	Research research3(research2);
	Research research4 = research2 + research3;

	std::cout << "\nResearch 2:\n";
	research2.printInfo();
	std::cout << "\nResearch 3 (copy):\n";
	std::cout << research3 << std::endl;
	std::cout << "\nResearch 4 (sum):\n";
	std::cout << research4 << std::endl;

	std::cout << "\nResearch 4 (old publications removed):\n";
	research4.removeOldPublications(std::chrono::system_clock::now() - std::chrono::days(8));
	std::cout << research4 << std::endl;

	return 0;
}
