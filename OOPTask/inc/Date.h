#pragma once

#include <chrono>

namespace date
{
	
	using Date = std::chrono::system_clock::time_point;

} // end namespace date
