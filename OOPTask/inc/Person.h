#pragma once

#include <iostream>
#include <string>

#include "Date.h"

namespace ooptask
{

	class Person
	{	
	public:
		Person() = default;
		Person(const std::string& aFirstName, const std::string& aLastName, const date::Date& aDate)
			:mFirstName(aFirstName), mLastName(aLastName), mBirthday(aDate) {};
		Person(const Person& aPerson) = default;
		virtual ~Person() = default;
		auto firstName() const -> std::string { return mFirstName; };
		auto firstName(const std::string& aFirstName) -> void { mFirstName = aFirstName; };
		auto lastName() const -> std::string { return mLastName; };
		auto lastName(const std::string& aLastName) -> void { mLastName = aLastName; };
		auto birthday() const -> date::Date { return mBirthday; };
		auto birthday(const date::Date& aBirthday) -> void { mBirthday = aBirthday; };
		auto printInfo() const -> void { std::cout << firstName() << " " << lastName() << std::endl; };
	private:
		std::string mFirstName;
		std::string mLastName;
		date::Date mBirthday;
	};

} // end namespace ooptask
