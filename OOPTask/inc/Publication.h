#pragma once

#include <iostream>

#include "Date.h"
#include "Student.h"
#include "ScientificAchievement.h"

namespace ooptask 
{
	
	class Publication
	{
	public:
		Publication() = default;
		Publication(const Student& aStudent, ScientificAchievement aScientificAchievement, const date::Date& aDate)
			:mStudent(aStudent), mScientificAchievement(aScientificAchievement), mDate(aDate) {};
		Publication(const Publication& aPublication) = default;
		virtual ~Publication() = default;
		auto student() const -> Student { return mStudent; };
		auto student(const Student& aStudent) -> void { mStudent = aStudent; };
		auto achievement() const -> ScientificAchievement { return mScientificAchievement; };
		auto achievement(ScientificAchievement aScientificAchievement) -> void { mScientificAchievement = aScientificAchievement; };
		auto date() const -> date::Date { return mDate; };
		auto date(const date::Date& aDate) -> void { mDate = aDate; };
		auto printInfo() const -> void { std::cout << student().firstName() << ", " << achievementToString(achievement()) << ", date = " << std::chrono::system_clock::to_time_t(date()) << std::endl; };
	private:
		Student mStudent;
		ScientificAchievement mScientificAchievement;
		date::Date mDate;
	};

}