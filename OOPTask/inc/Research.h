#pragma once

#include <deque>
#include <string>

#include "Publication.h"

namespace ooptask
{

	class Research
	{
	public:
		Research();
		Research(const std::deque<Publication>& aPublications, const std::string& aTopic);
		Research(const Research& aResearch);
		virtual ~Research();
		auto publications() const -> std::deque<Publication>;
		auto publications(const std::deque<Publication>& aPublications) -> void;
		auto topic() const -> std::string;
		auto topic(const std::string& aTopic) -> void;
		auto addPublication(const Publication& aPublication) -> void;
		auto removeOldPublications(const date::Date& aDate) -> void;
		auto printInfo() const -> void;
		auto operator+=(const Research& aRes) -> Research&;
		friend auto operator+(Research aRes1, const Research& aRes2)->Research;
		friend auto operator<<(std::ostream& aOut, const Research& aResearch)->std::ostream&;
	private:
		std::deque<Publication> mPublications;
		std::string mTopic;
	};

}