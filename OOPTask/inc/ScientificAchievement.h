#pragma once

namespace ooptask
{

	enum class ScientificAchievement
	{
		Abstract,
		ProfessionalPublicationArticle,
		ScientificJournalArticle,
		InternationalConferenceReport,
	};

	constexpr auto achievementToString(ScientificAchievement aScientificAchievement) -> const char*
	{
		switch (aScientificAchievement)
		{
		case ScientificAchievement::Abstract: return "Abstract";
		case ScientificAchievement::ProfessionalPublicationArticle: return "Professional publication article";
		case ScientificAchievement::ScientificJournalArticle: return "Scientific journal article";
		case ScientificAchievement::InternationalConferenceReport: return "International conference report";
		}
		return "Other";
	}

} // end namespace ooptask
