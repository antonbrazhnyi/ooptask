#pragma once

#include "Person.h"

namespace ooptask
{

	class Student : public Person
	{
	public:
		Student() = default;
		Student(const std::string& aFirstName, const std::string& aLastName, const date::Date& aDate, int aYear)
			: Person(aFirstName, aLastName, aDate), mYear(aYear) {};
		Student(const Student& aStudent) = default;
		virtual ~Student() = default;
		auto year() const -> int { return mYear; };
		auto year(int aYear) -> void { mYear = aYear; };
		auto printInfo() const -> void { std::cout << firstName() << " " << lastName() << ", year =  " << year() << std::endl; };
	private:
		int mYear;
	};

} // end namespace ooptask
