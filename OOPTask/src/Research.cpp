#include "Research.h"

#include <unordered_map>
#include <deque>

namespace ooptask
{

	Research::Research() = default;

	Research::Research(const std::deque<Publication>& aPublications, const std::string& aTopic)
		:mPublications(aPublications), mTopic(aTopic)
	{

	}

	Research::Research(const Research& aResearch) = default;

	Research::~Research() = default;

	auto Research::publications() const -> std::deque<Publication>
	{
		return mPublications;
	}

	auto Research::publications(const std::deque<Publication>& aPublications) -> void
	{
		mPublications = aPublications;
	}

	auto Research::topic() const -> std::string
	{
		return mTopic;
	}

	auto Research::topic(const std::string& aTopic) -> void
	{
		mTopic = aTopic;
	}

	auto Research::addPublication(const Publication& aPublication) -> void
	{
		mPublications.push_front(aPublication);
	}

	auto Research::removeOldPublications(const date::Date& aDate) -> void
	{
		std::erase_if(mPublications, [aDate](Publication p) { return p.date() < aDate; });
	}

	auto Research::printInfo() const -> void
	{
		std::cout << "Topic: " << topic() << '\n';
		std::cout << "Publications:\n";
		for (const Publication& publication: publications())
		{
			publication.printInfo();
		}
	}

	auto Research::operator+=(const Research& aResearch) -> Research&
	{
		for (const Publication& publication : publications())
		{
			addPublication(publication);
		}
		return *this;
	}

	auto operator+(Research aRes1, const Research& aRes2) -> Research
	{
		aRes1 += aRes2;
		return aRes1;
	}

	auto operator<<(std::ostream& aOut, const Research& aResearch) -> std::ostream&
	{
		aOut << "Topic: " << aResearch.topic() << '\n';
		std::unordered_map<ScientificAchievement, int> counter;
		for (Publication publication : aResearch.publications())
		{
			counter[publication.achievement()]++;
		}
		for (auto it = counter.cbegin(); it != counter.cend(); ++it)
		{
			aOut << achievementToString(it->first) << ": " << it->second << '\n';
		}
		return aOut;
	}

}